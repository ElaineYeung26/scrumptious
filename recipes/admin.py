from django.contrib import admin
from recipes.models import Recipe, RecipeStep, Ingredient

# Register your models here.
@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = [
        "title",
        "id",
        "description",
        "created_on"
    ]


@admin.register(RecipeStep)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = [
        "step_number",
        "id",
        "instruction",
    ]


@admin.register(Ingredient)
class IngredientAdmin(admin.ModelAdmin):
    list_display = [
        "amount",
        "id",
        "food_item",
    ]
